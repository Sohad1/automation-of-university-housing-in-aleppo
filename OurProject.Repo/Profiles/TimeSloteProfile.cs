﻿using AutoMapper;
using DataBase.Entities;
using OurProject.Dto.TimeSloteDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OurProject.Repo.Profiles
{
   public class TimeSloteProfile:Profile
    {
        public TimeSloteProfile()
        {
            CreateMap<TimeSloteEntity, AddTimeSloteDto>();
            CreateMap<AddTimeSloteDto, TimeSloteEntity>();


            CreateMap<TimeSloteEntity, GetAllTimeSloteDto>()
              .ForMember(des => des.id, src => src.MapFrom(c => c.Id));
            CreateMap<GetAllTimeSloteDto, TimeSloteEntity>()
             .ForMember(des => des.Id, src => src.MapFrom(c => c.id));
        }
    }
}
