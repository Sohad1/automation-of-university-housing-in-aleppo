﻿using AutoMapper;
using DataBase.Entities;
using OurProject.Dto.StudentDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OurProject.Repo.Profiles
{
   public class StudentProfile:Profile
    {
        public StudentProfile()
        {
            CreateMap<StudentEntity, AddStudentDto>();
            CreateMap<AddStudentDto, StudentEntity>();


            CreateMap<StudentEntity, GetAllStudentDto>()
              .ForMember(des => des.id, src => src.MapFrom(c => c.Id));
            CreateMap<GetAllStudentDto, StudentEntity>()
             .ForMember(des => des.Id, src => src.MapFrom(c => c.id));
        }
    }
}
