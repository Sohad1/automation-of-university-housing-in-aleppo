﻿using AutoMapper;
using DataBase;
using DataBase.Entities;
using OurProject.Dto.TimeSloteDto;
using OurProject.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OurProject.Repo
{
    public class TimeSloteRepo : TimeSloteIRepo
    {
        private readonly IMapper mapper;
        private MyDbContext dbContext { get; set; }
        public TimeSloteRepo(IMapper pmapper)
        {
            dbContext = new MyDbContext();
            mapper = pmapper;
        }
        public bool AddTimeSlote(AddTimeSloteDto dto)
        {
            try
            {
                dbContext.Add(mapper.Map<TimeSloteEntity>(dto));
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteTimeSlote(int id)
        {
            try
            {
                var result = dbContext.timeSlotes.FirstOrDefault(c => c.Id == id);
                dbContext.Remove(result);
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<GetAllTimeSloteDto> GetAllTimeSlote()
        {
            try
            {
                var result = dbContext.timeSlotes.ToList();
                return mapper.Map<List<GetAllTimeSloteDto>>(result);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateTimeSlote(AddTimeSloteDto dto, int id)
        {
            try
            {
                var result = dbContext.timeSlotes.FirstOrDefault(c => c.Id == id);
                if (result != null)
                {
                    if (dto.slote!=DateTime.MinValue)
                    {
                        result.slote = dto.slote;
                    }
                    dbContext.Update(result);
                    dbContext.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
