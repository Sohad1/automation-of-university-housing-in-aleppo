﻿using AutoMapper;
using DataBase;
using DataBase.Entities;
using OurProject.Dto.StudentDto;
using OurProject.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OurProject.Repo
{
    public class StudentRepo : StudentIRepo
    {
        private readonly IMapper mapper;
        private MyDbContext dbContext { get; set; }
        public StudentRepo(IMapper pmapper)
        {
            dbContext = new MyDbContext();
            mapper = pmapper;
        }
        public bool AddStudent(AddStudentDto dto)
        {
            try
            {
                dbContext.Add(mapper.Map<StudentEntity>(dto));
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteStudent(int id)
        {
            try
            {
                var result = dbContext.students.FirstOrDefault(c => c.Id == id);
                dbContext.Remove(result);
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<GetAllStudentDto> GetAllStudent()
        {
            try
            {
                var result = dbContext.students.ToList();
                return mapper.Map<List<GetAllStudentDto>>(result);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateStudent(AddStudentDto dto, int id)
        {
            try
            {
                var result = dbContext.students.FirstOrDefault(c => c.Id == id);
                if (result != null)
                {
                    if (dto.CityId != 0)
                    {
                        result.CityId = dto.CityId;
                    }
                    
                    result.Gender = dto.Gender;

                    if (!string.IsNullOrEmpty(dto.Image))
                    {
                        result.Image = dto.Image;
                    }
                    if (!string.IsNullOrEmpty(dto.Name))
                    {
                        result.Name = dto.Name;
                    }  
                    if (!string.IsNullOrEmpty(dto.NationalId))
                    {
                        result.NationalId = dto.NationalId;
                    }  

                    result.Recorded = dto.Recorded;

                    if (dto.StudyBranchId!=0)
                    {
                        result.StudyBranchId = dto.StudyBranchId;
                    } 
                    if (dto.UnitRoomFk!=0)
                    {
                        result.UnitRoomFk = dto.UnitRoomFk;
                    } 
                    if (!string.IsNullOrEmpty(dto.UniversityId))
                    {
                        result.UniversityId = dto.UniversityId;
                    }
                    dbContext.Update(result);
                    dbContext.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
