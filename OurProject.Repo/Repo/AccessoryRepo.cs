﻿
using AutoMapper;
using DataBase;
using DataBase.Entities;
using OurProject.Dto.AccessoryDto;
using OurProject.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OurProject.Repo
{
    public class AccessoryRepo : AccessoryIRepo
    {
        private readonly IMapper mapper;
        private MyDbContext dbContext { get; set; }
        public AccessoryRepo(IMapper pmapper)
        {
            dbContext = new MyDbContext();
            mapper = pmapper;
        }
        public bool AddAccessory(AddAccessoryDto dto)
        {
            try
            {
                dbContext.Add(mapper.Map<AccessoryEntity>(dto));
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteAccessory(int id)
        {
            try
            {
                var result = dbContext.accessories.FirstOrDefault(c => c.Id == id);
                dbContext.Remove(result);
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<GetAllAccessoryDto> GetAllAccessory()
        {
            try
            {
                var result = dbContext.accessories.ToList();
                return mapper.Map<List<GetAllAccessoryDto>>(result);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateAccessory(AddAccessoryDto dto, int id)
        {
            try
            {
                var result = dbContext.accessories.FirstOrDefault(c => c.Id == id);
                if (result != null)
                {
                    if (!string.IsNullOrEmpty(dto.Name))
                    {
                        result.Name = dto.Name;
                    }
                    dbContext.Update(result);
                    dbContext.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
