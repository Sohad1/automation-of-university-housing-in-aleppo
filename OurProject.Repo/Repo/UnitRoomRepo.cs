﻿using AutoMapper;
using DataBase;
using DataBase.Entities;
using OurProject.Dto.UnitRoomDto;
using OurProject.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OurProject.Repo
{
    public class UnitRoomRepo : UnitRoomIRepo
    {
        private readonly IMapper mapper;
        private MyDbContext dbContext { get; set; }
        public UnitRoomRepo(IMapper pmapper)
        {
            dbContext = new MyDbContext();
            mapper = pmapper;
        }
        public bool AddUnitRoom(AddUnitRoomDto dto)
        {
            try
            {
                dbContext.Add(mapper.Map<UnitRoomEntity>(dto));
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteUnitRoom(int id)
        {
            try
            {
                var result = dbContext.unitRooms.FirstOrDefault(c => c.Id == id);
                dbContext.Remove(result);
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<GetAllUnitRoomDto> GetAllUnitRoom()
        {
            try
            {
                var result = dbContext.unitRooms.ToList();
                return mapper.Map<List<GetAllUnitRoomDto>>(result);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateUnitRoom(AddUnitRoomDto dto, int id)
        {
            try
            {
                var result = dbContext.unitRooms.FirstOrDefault(c => c.Id == id);
                if (result != null)
                {
                    if (dto.RoomId!=0)
                    {
                        result.RoomId = dto.RoomId;
                    }
                    if (dto.StudentCount != -1)
                    {
                        result.StudentCount = dto.StudentCount;
                    }
                    if (dto.UnitId != 0)
                    {
                        result.UnitId = dto.UnitId;
                    }
                    dbContext.Update(result);
                    dbContext.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
