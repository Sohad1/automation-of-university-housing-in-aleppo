﻿using AutoMapper;
using DataBase;
using DataBase.Entities;
using OurProject.Dto.StudentAccessoriesDto;
using OurProject.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OurProject.Repo
{
    public class StudentAccessoriesRepo : StudentAccessoriesIRepo
    {
        private readonly IMapper mapper;
        private MyDbContext dbContext { get; set; }
        public StudentAccessoriesRepo(IMapper pmapper)
        {
            dbContext = new MyDbContext();
            mapper = pmapper;
        }
        public bool AddStudentAccessories(AddStudentAccessoriesDto dto)
        {
            try
            {
                dbContext.Add(mapper.Map<StudentAccessoriesEntity>(dto));
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteStudentAccessories(int id)
        {
            try
            {
                var result = dbContext.studentAccessories.FirstOrDefault(c => c.Id == id);
                dbContext.Remove(result);
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<GetAllStudentAccessoriesDto> GetAllStudentAccessories()
        {
            try
            {
                var result = dbContext.studentAccessories.ToList();
                return mapper.Map<List<GetAllStudentAccessoriesDto>>(result);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateStudentAccessories(AddStudentAccessoriesDto dto, int id)
        {
            try
            {
                var result = dbContext.studentAccessories.FirstOrDefault(c => c.Id == id);
                if (result != null)
                {
                    if (dto.Amount!=-1)
                    {
                        result.Amount = dto.Amount;
                    }  

                    result.Returned = dto.Returned;

                    if (dto.StoreDetailId!=0)
                    {
                        result.StoreDetailId = dto.StoreDetailId;
                    }
                    if (dto.StudentId!=0)
                    {
                        result.StudentId = dto.StudentId;
                    }
                    dbContext.Update(result);
                    dbContext.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
