﻿//validation
const form = document.getElementById('form');
const name = document.getElementById('name');
const Roll = document.getElementById('Roll');
const PhoneNumber = document.getElementById('PhoneNumber');
const Date = document.getElementById('Date');




function checkInputs() {
    const nameValue = name.value.trim();
    const RollValue = Roll.value;
    const PhoneNumberValue = PhoneNumber.value.trim();
    const DateValue = Date.value;
    var b = 0;

    if (nameValue === '') {
        setErrorFor(name, 'الرجاء أدخال اسم الوحدة');
    } 
    else {
        setSuccessFor(name);
        b = b + 1;
    }
    if (RollValue == -1) {
        setErrorFor(Roll, 'الرجاء أختيار اسم المشرف');
    } else {
        setSuccessFor(Roll);
        b = b + 1;
    }
    if (PhoneNumberValue === '') {
        setErrorFor(PhoneNumber, 'الرجاء أدخال رقم الهاتف');
    } else {
        setSuccessFor(PhoneNumber);
        b = b + 1;
    }
    if (DateValue === '') {
        setErrorFor(Date, 'الرجاء أدخال تاريخ التعيين');
    } else {
        setSuccessFor(Date);
        b = b + 1;
    }
    
    if (b == 4) {
        return true;
    }
    else {
        return false;
    }
}

function setErrorFor(input, message) {
    const big = input.parentElement;
    const small = big.querySelector('small');

    small.innerText = message;
    big.className = 'big error';
}
function setSuccessFor(input) {
    const big = input.parentElement;
    big.className = 'big correct';
}