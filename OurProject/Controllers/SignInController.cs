﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OurProject.Dto.UserDto;
using OurProject.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OurProject.Controllers
{
    public class SignInController : Controller
    {
        private readonly ILogger<SignInController> _logger;
        private readonly UserIRepo _userRepo;
        private readonly UnitIRepo _unitIRepo;

        public SignInController
            (
            ILogger<SignInController> logger, UserIRepo userRepo
            , UnitIRepo unitIRepo
            )
        {
            _logger = logger;
            _userRepo = userRepo;
            _unitIRepo = unitIRepo;
        }
        public IActionResult SignInBtn(UserLogInDto logInDto)
        {
            if (!ModelState.IsValid)
            {
                return View("SignInView");
            }
            var user = _userRepo.GetUser(logInDto);
            if (user != null)
            {
                Program.CurrentUser = user;
                if (user.RoleId == 1)
                {
                    return View("~/Views/Maneger/MainManegerView.cshtml", _unitIRepo.GetAllUnitInfo());
                }
                if (user.RoleId == 2)
                {
                    return View("SignInView");
                }
                if (user.RoleId == 3)
                {
                    return View("SignInView");
                }
                else 
                {
                    return Unauthorized();
                }
            }
            else 
            { 
                return NotFound();
            }
          
               
            
        }
    }
}
