﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OurProject.Dto.UnitDto;
using OurProject.Dto.UserDto;
using OurProject.IRepo;
using OurProject.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace OurProject.Controllers
{
    public class ManegerController : Controller
    {
        private readonly ILogger<ManegerController> _logger;
        private readonly UnitIRepo _unitIRepo;
        private readonly UserIRepo _userIRepo;
        private readonly RoleIRepo _roleIRepo;
        private readonly StudyBranchIRepo _studyBranchIRepo;
        private readonly UnitStoreIRepo _unitStoreIRepo;
        public ManegerController
           (
           ILogger<ManegerController> logger, UnitIRepo unitIRepo
            , UserIRepo userIRepo, RoleIRepo roleIRepo, StudyBranchIRepo studyBranchIRepo
            , UnitStoreIRepo unitStoreIRepo
           )
        {
            _logger = logger;
            _unitIRepo = unitIRepo;
            _userIRepo = userIRepo;
            _roleIRepo = roleIRepo;
            _studyBranchIRepo = studyBranchIRepo;
            _unitStoreIRepo = unitStoreIRepo;
        }
        public IActionResult HomeBtn()
        {
            var x = _unitIRepo.GetAllUnitInfo();
            return View("MainManegerView",x);
        }
        public IActionResult SuperVisorSettingBtn()
        {
            UpdateSuperVisorDto updateSuperVisorDto = new UpdateSuperVisorDto()
            {
                getSuperVisorInfos = _userIRepo.GetSuperVisorInfo(),
            };
            Program.JavascriptToRun = "";
            return View("SuperVisorSettingView", updateSuperVisorDto);
        }   
        public IActionResult UpdateSuperVisor(UpdateSuperVisorDto updateSuperVisorDto)
        {

            if (_userIRepo.UpdateUser(updateSuperVisorDto.UserDto, updateSuperVisorDto.UserId)) 
            {
                UpdateSuperVisorDto superVisorDto  = new UpdateSuperVisorDto()
                {
                    getSuperVisorInfos = _userIRepo.GetSuperVisorInfo(),
                };
                Program.JavascriptToRun = "toastr.success('تم التعديل بنجاح')";
                return View("SuperVisorSettingView", superVisorDto);
            }
            else
            {
                UpdateSuperVisorDto superVisorDto = new UpdateSuperVisorDto()
                {
                    getSuperVisorInfos = _userIRepo.GetSuperVisorInfo(),
                };
                Program.JavascriptToRun = "toastr.error('حدث خطأ في عملية التعديل')";
                return View("SuperVisorSettingView", superVisorDto);
            }


        }
        public IActionResult DeleteSuperVisor(UpdateSuperVisorDto updateSuperVisorDto)
        {
            if (_userIRepo.DeleteUser(updateSuperVisorDto.UserId))
            {
                UpdateSuperVisorDto superVisorDto = new UpdateSuperVisorDto()
                {
                    getSuperVisorInfos = _userIRepo.GetSuperVisorInfo(),
                };
                Program.JavascriptToRun = "toastr.success('تم الحذف بنجاح')";
                return View("SuperVisorSettingView", superVisorDto);
            }
            else
            {
                UpdateSuperVisorDto superVisorDto = new UpdateSuperVisorDto()
                {
                    getSuperVisorInfos = _userIRepo.GetSuperVisorInfo(),
                };
                Program.JavascriptToRun = "toastr.error('حدث خطأ في عملية الحذف')";
                return View("SuperVisorSettingView", superVisorDto);
            }


        }
        public IActionResult AddUserBtn()
        {
            AddNewUserDto addNewUserDto = new AddNewUserDto
            {
                getAllRoles = _roleIRepo.GetAllRole(),
                
            };
            addNewUserDto.getAllRoles=addNewUserDto.getAllRoles.FindAll(x => x.id != 1);
            return View("AddUserView", addNewUserDto);
        } 
        public IActionResult AddUser(AddNewUserDto newUserDto)
        {
            newUserDto.userDto.Password ="-1";
            if (newUserDto.userDto.RoleId==2)
            {
                if (_userIRepo.AddUser(newUserDto.userDto))
                {
                    UpdateSuperVisorDto superVisorDto = new UpdateSuperVisorDto()
                    {
                        getSuperVisorInfos = _userIRepo.GetSuperVisorInfo(),
                    };
                    Program.JavascriptToRun = "";
                    return View("SuperVisorSettingView", superVisorDto);
                }
            }
            else if (newUserDto.userDto.RoleId == 3)
            {
                if (_userIRepo.AddUser(newUserDto.userDto))
                {
                    UpdateStoreKeeperDto updateStoreKeeperDto = new UpdateStoreKeeperDto()
                    {
                        getStoreKeeperInfos = _userIRepo.GetStoreKeeperInfo(),
                    };
                    Program.JavascriptToRun = "";
                    return View("StoreKeeperSettingView", updateStoreKeeperDto);
                }
            }
           
            return View("xView");

            //newUserDto.userDto.Password = "-1";
            //if (newUserDto.UnitId==-1)
            //{
            //    if (_userIRepo.AddUser(newUserDto.userDto))
            //    {
            //        return View("SuperVisorSettingView", _userIRepo.GetSuperVisorInfo());
            //    }
            //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
            //}
            //else
            //{
            //    int lastUser = _userIRepo.AddUserWithIdReturn(newUserDto.userDto);
            //    if (lastUser!=-1)
            //    {
            //        AddUnitDto newUnite = _unitIRepo.GetOneUnit(newUserDto.UnitId);
            //        if (newUserDto.userDto.RoleId==2)
            //        {
            //            newUnite.UserSuperVisorFk = lastUser;
            //            _unitIRepo.UpdateUnit(newUnite, newUserDto.UnitId);
            //            return View("SuperVisorSettingView", _userIRepo.GetSuperVisorInfo());
            //        }
            //        else 
            //        {
            //            newUnite.UserUnitStoreKeeperFk = lastUser;
            //            _unitIRepo.UpdateUnit(newUnite, newUserDto.UnitId);
            //            return View("StoreKeeperSettingView", _userIRepo.GetStoreKeeperInfo());
            //        }

            //    }
            //    return View("MainManegerView", _unitIRepo.GetAllUnitInfo());
            //}

        }  
        public IActionResult StoreKeeperSettingBtn()
        {
            UpdateStoreKeeperDto updateStoreKeeperDto = new UpdateStoreKeeperDto()
            {
                getStoreKeeperInfos = _userIRepo.GetStoreKeeperInfo(),
            };
            Program.JavascriptToRun = "";
            return View("StoreKeeperSettingView", updateStoreKeeperDto);
        }
        public IActionResult UpdateStoreKeeper(UpdateStoreKeeperDto updateStoreKeeperDto)
        {

            if (_userIRepo.UpdateUser(updateStoreKeeperDto.UserDto, updateStoreKeeperDto.UserId))
            {
                UpdateStoreKeeperDto storeKeeperDto = new UpdateStoreKeeperDto()
                {
                    getStoreKeeperInfos = _userIRepo.GetStoreKeeperInfo(),
                    
            };
                Program.JavascriptToRun ="toastr.success('تم التعديل بنجاح')";
                return View("StoreKeeperSettingView", storeKeeperDto);
            }
            else
            {
                UpdateStoreKeeperDto storeKeeperDto = new UpdateStoreKeeperDto()
                {
                    getStoreKeeperInfos = _userIRepo.GetStoreKeeperInfo(),
                };
                Program.JavascriptToRun = "toastr.error('حدث خطأ في عملية التعديل')";
                return View("StoreKeeperSettingView", storeKeeperDto);
            }


        }
        public IActionResult DeleteStoreKeeper(UpdateStoreKeeperDto updateStoreKeeperDto)
        {
            if (_userIRepo.DeleteUser(updateStoreKeeperDto.UserId))
            {
                UpdateStoreKeeperDto storeKeeperDto = new UpdateStoreKeeperDto()
                {
                    getStoreKeeperInfos = _userIRepo.GetStoreKeeperInfo(),
                };
                Program.JavascriptToRun ="toastr.success('تم الحذف بنجاح')";
                return View("StoreKeeperSettingView", storeKeeperDto);
            }
            else
            {
                UpdateStoreKeeperDto storeKeeperDto = new UpdateStoreKeeperDto()
                {
                    getStoreKeeperInfos = _userIRepo.GetStoreKeeperInfo(),
                };
                Program.JavascriptToRun = "toastr.error('حدث خطأ في عملية الحذف')";
                return View("StoreKeeperSettingView", storeKeeperDto);
            }


        }
        public IActionResult AddUnitBtn()
        {
            AddNewUnitDto addNewUnitDto = new AddNewUnitDto
            {
                StoreKeepers = _userIRepo.GetFreeStoreKeeper(),
                SuperVisors = _userIRepo.GetFreeSuperVisor(),
                studyBranchs = _studyBranchIRepo.GetAllStudyBranch(),
                unitStores = _unitStoreIRepo.GetFreeStore()
            };

            return View("AddUnitView", addNewUnitDto);
        }
        public IActionResult AddUnit(AddNewUnitDto newUnitDto)
        {

            //AddNewUnitDto addNewUnitDto = new AddNewUnitDto
            //{
            //    StoreKeepers = _userIRepo.GetFreeStoreKeeper(),
            //    SuperVisors = _userIRepo.GetFreeSuperVisor(),
            //    studyBranchs = _studyBranchIRepo.GetAllStudyBranch(),
            //    unitStores = _unitStoreIRepo.GetFreeStore()
            //};
            //if (!ModelState.IsValid)
            //{
            //    return View("AddUnitView", addNewUnitDto);
            //}
            if (_unitIRepo.AddUnit(newUnitDto.UnitDto))
            {
                return View("MainManegerView", _unitIRepo.GetAllUnitInfo());
            }
            else
            {
                return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
            }
           
        }  
        public IActionResult EditUnit(int id)
        {
            UpdateUnitDto addNewUnitDto = new UpdateUnitDto
            {
                StoreKeepers = _userIRepo.GetFreeStoreKeeper(),
                SuperVisors = _userIRepo.GetFreeSuperVisor(),
                studyBranchs = _studyBranchIRepo.GetAllStudyBranch(),
                unitStores = _unitStoreIRepo.GetFreeStore(),
                UnitDto= _unitIRepo.GetSpecificUnit(id)
            };
            return View("EditUnitView", addNewUnitDto);
        }    
        public IActionResult UpdateUnit(UpdateUnitDto updateUnitDto)
        {
            if (updateUnitDto.male)
            {
                updateUnitDto.AddUnitDto.Gender = updateUnitDto.male;
            }
            else
            {
                updateUnitDto.AddUnitDto.Gender = false;
            }
            var d = _unitIRepo.UpdateUnit(updateUnitDto.AddUnitDto, updateUnitDto.UnitId);
            if (d)
            {
                return View("MainManegerView", _unitIRepo.GetAllUnitInfo());
            }
            else
            {
                return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
            }

        }
        public IActionResult x()
        {
            return View("xView");
        }
    }
}
