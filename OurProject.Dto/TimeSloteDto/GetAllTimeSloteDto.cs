﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OurProject.Dto.TimeSloteDto
{
    public class GetAllTimeSloteDto
    {
        public int id { get; set; }
        public DateTime slote { get; set; }
    }
}
