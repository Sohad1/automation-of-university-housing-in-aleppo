﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OurProject.Dto.AccessoryDto
{
   public class AddAccessoryDto
    {
        public String Name { get; set; }
    }
}
